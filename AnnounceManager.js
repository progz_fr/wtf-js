function AnnounceManager()
{
    var _this = this;
    setInterval(function(){ _this.tick(); }, 30000);

    this.announces = []
    this.div = document.createElement("div");
    this.div.classList.add("announce_bg");
    this.div2 = document.createElement("div");  // Div que j'ai voulu ajouter qui fait la moitié de la div mère, elle n'existait pas au préalable
    this.div2.classList.add("announce"); 

    this.div.appendChild(this.div2);
    document.body.appendChild(this.div);

}

AnnounceManager.prototype.addAnnounce = function(background, content)
{
    var announce = {background: background, content: content}
    this.announces.push(announce);
}

AnnounceManager.prototype.tick = function()
{
    //next announce
    var _this = this;
    var jdiv = $(this.div);
    jdiv.fadeOut(1500,function(){
        setTimeout(function(){
            if(_this.announces.length > 0){
                var announce = _this.announces[0];
                _this.announces.splice(0,1);
                _this.div.style.backgroundImage = "url('"+announce.background+"')"; // Je laisse le background dans Div1
                _this.div2.innerHTML = announce.content; // Je met le contenu dans la Div2

                jdiv.fadeIn(800,function(){});
            }
        }, 2000);
    });
}